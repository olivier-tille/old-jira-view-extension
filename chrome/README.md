# Old Jira View - Chrome Extension

Chrome Extension that automatically redirects from the new Jira issue page to the old one.

## Installation
Install the unpacked extensions by:

1. download the extension from the downloads page (Download repository): [https://bitbucket.org/olivier-tille/old-jira-view-extension/downloads/](https://bitbucket.org/olivier-tille/old-jira-view-extension/downloads/)
2. unzip the downloaded Zip-File
3. if not already activated, activate the developer mode at: chrome://extensions
4. install the unpacked extentions through the "Load unpacked" button
