chrome.webRequest.onBeforeRequest.addListener(
    function(details) {
        if (details.url.indexOf('.jira.com/browse/') !== -1 
            && details.url.indexOf('oldIssueView') === -1) {
            return {
                redirectUrl: details.url + '?oldIssueView=true'
            };
        }
        return {};
    }, 
    {
        urls : ["<all_urls>"]
    }, 
    ["blocking"]
);
